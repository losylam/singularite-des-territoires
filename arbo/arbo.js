var arbo = {
    "label": "Réel",
    "group": "0xffffff",
    "rang": 1,
    "synonymes": ["Vrai", "Tangible", "Réel", "vrai", "tangible", "réel", "reel"],
    "définition": "Ensemble de l'univers environnant, perceptible et cartographiable.",
    "children": [
        {
            "label": "Formes Physiques",
            "group": 0x3A7945,
            "rang": 2,
            "posx": 263,
            "posy": 0,
            "posz": 0,
            "synonymes": ["Formes Physiques", "Matériel", "concret", "nature"],
            "définition": "Qui a trait à la matière, à la nature, aux corps en général, à la réalité matérielle perceptible par les sens ou qui peut être observé objectivement.",
            "children": [
                {
                    "label": "Bios",
                    "rang": 3,
                    "posx": 475,
                    "posy": 0,
                    "posz": 135,
                    "group": 0x3A7945,
                    "children": [
                        {
                            "label": "Nécessaire",
                            "group": 0x3A7945,
                            "rang": 4,
                            "posx": 810,
                            "posy": 240,
                            "posz": 370,
                            "définition": "Continuation stable du monde vivant. Ce qui constitue l'enchaînement inéluctable de la vie.",
                            "children": [
                                {
                                    "label": "Hérédité",
                                    "group": 0x3A7945,
                                    "rang": 5,
                                    "posx": 1250,
                                    "posy": 430,
                                    "posz": 670,
                                    "synonymes": ["Hérédité", "Parenté", "héritage"],
                                    "définition": "Perpétuation de caractères hérités. Un être vivant né et grandit grace à un patrimoine génétique qui détermine les caractéristiques physiques de l'individu.",
                                    "children": [
                                        {
                                            "label": "Activation",
                                            "group": 0x3A7945,
                                            "rang": 6,
                                            "posx": 1700,
                                            "posy": 340,
                                            "posz": 670,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Activation</b><br/>Reproduction sexuée<br/>Reproduction assexuée<br/>Cathalyse<br/>Greffes, Transplantation <br/>Extinctions génétiques<br/><b>Inhibition</b>",
                                            "définition": "L'ensemble des caractères hérités qui se développe, se manifestant sur la génération en cours."
                                        },
                                        {
                                            "label": "Inhibition",
                                            "group": 0x3A7945,
                                            "rang": 6,
                                            "posx": 1630,
                                            "posy": 650,
                                            "posz": 670,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Activation</b><br/>Reproduction sexuée<br/>Reproduction assexuée<br/>Cathalyse<br/>Greffes, Transplantation <br/>Extinctions génétiques<br/><b>Inhibition</b>",
                                            "définition": "L'ensemble des caractères hérités qui ne se développe pas."
                                        }

                                    ]

                                },
                                {
                                    "label": "Développement",
                                    "group": 0x3A7945,
                                    "rang": 5,
                                    "posx": 1145,
                                    "posy": 230,
                                    "posz": 975,
                                    "synonymes": ["Développement", "Croissance", "processus"],
                                    "définition": "A partir du patrimoine génétique qui a constitué un être vivant, celui ci se dévelope, grandit, s'épanouit, évolue sous l'action des lois naturelles de la vie.",
                                    "children": [
                                        {
                                            "label": "Préservation",
                                            "group": 0x3A7945,
                                            "rang": 6,
                                            "posx": 1250,
                                            "posy": 675,
                                            "posz": 1200,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Preservation</b><br/>Préservation environmentale<br/>Agriculture<br/>Elevage<br/><b>Exploitation</b>",
                                            "synonymes": ["Préservation", "Protection", "Sauvegarde", "Conservation"],
                                            "définition": "Protection garantie de certains écosystèmes."
                                        },
                                        {
                                            "label": "Exploitation",
                                            "group": 0x3A7945,
                                            "rang": 6,
                                            "posx": 1160,
                                            "posy": 340,
                                            "posz": 1400,
                                            "cartabs": "<b>Preservation</b><br/>Préservation environmentale<br/>Agriculture<br/>Elevage<br/><b>Exploitation</b>",
                                            "définition": "Tirer profit du développement, le faire valoir."

                                        }

                                    ]
                                }
                            ]
            },
                        {
                            "label": "Hasard",
                            "group": 0x3A7945,
                            "rang": 4,
                            "posx": 810,
                            "posy": -250,
                            "posz": 370,
                            "définition": "L’ensemble des événements imprévisibles, de liberté absolue mais aveugle. Le vivant possède par essence une certaine capacité à évoluer aléatoirement dans un cadre invariant.",
                            "children": [
                                {
                                    "label": "Adaptation",
                                    "group": 0x3A7945,
                                    "rang": 5,
                                    "posx": 1245,
                                    "posy": -490,
                                    "posz": 670,
                                    "synonymes": ["Adaptation", "Accommodation", "acclimatation"],
                                    "définition": "Modifications morphologiques, permettant la survie d'un individu ou d'une espèce suite à des modifications externes ou propre à elle même.",
                                    "children": [
                                        {
                                            "label": "Métamorphose",
                                            "group": 0x3A7945,
                                            "rang": 6,
                                            "posx": 1600,
                                            "posy": -660,
                                            "posz": 670,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Métamorphose</b><br/>Développement indirect<br/>Polyallélisme<br/>OGM<br/>Fixation synthétique<br/><b>Fixation</b>",
                                            "définition": "Ensemble des modifications morphologiques subies par un être face au hasard."
                                        },
                                        {
                                            "label": "Fixation",
                                            "group": 0x3A7945,
                                            "rang": 6,
                                            "posx": 1700,
                                            "posy": -360,
                                            "posz": 670,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Métamorphose</b><br/>Développement indirect<br/>Polyallélisme<br/>OGM<br/>Fixation synthétique<br/><b>Fixation</b>",
                                            "définition": "Evolution d'un caractère, puis maintient de ce dernier de façon permanente."
                                        }
                                    ]

                                },
                                {
                                    "label": "Extinction",
                                    "group": 0x3A7945,
                                    "rang": 5,
                                    "posx": 1145,
                                    "posy": -250,
                                    "posz": 960,
                                    "synonymes": ["Extinction", "Destruction", "anéantissement", "fin", "disparition"],
                                    "définition": "Interruption de la vie face à une perturbation extérieure.",
                                    "children": [
                                        {
                                            "label": "Décomposition",
                                            "group": 0x3A7945,
                                            "rang": 6,
                                            "posx": 1200,
                                            "posy": -190,
                                            "posz": 1400,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Décomposition</b><br/>Destruction<br/>Altération<br/>Renouvellement<br/><b>Regénération</b>",
                                            "synonymes": ["Décomposition", "Dégradation", "destruction", "corruption", "désagrégation", "pourriture", "déliquescence", "putréfaction", "altération", "dissolution"],
                                            "définition": "Action de mener à son terme; état de ce qui est arrivé à son terme"
                                        },
                                        {
                                            "label": "Regénération",
                                            "group": 0x3A7945,
                                            "rang": 6,
                                            "posx": 1300,
                                            "posy": -510,
                                            "posz": 1220,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Décomposition</b><br/>Destruction<br/>Altération<br/>Renouvellement<br/><b>Regénération</b>",
                                            "définition": "Remplacement d'espèces par d'autres plus adaptées face au hasard."
                                        }
                                    ]
                                }
                            ]
            }

          ]
        },
                {
                    "label": "Gé",
                    "group": 0x3A7945,
                    "rang": 3,
                    "posx": 475,
                    "posy": 0,
                    "posz": -135,
                    "définition": "Le monde minéral, lié à la géologie. Toute forme inerte, qui est mue par une force extérieure.",
                    "children": [
                        {
                            "label": "Continuité",
                            "group": 0x3A7945,
                            "rang": 4,
                            "posx": 810,
                            "posy": -240,
                            "posz": -370,
                            "définition": "Continuité uniforme, propriété pour une fonction d'être uniformément continue. Philosophie. Principe de continuité, selon Leibniz, principe fondamental selon lequel l'évolution des êtres se produit de façon insensible et sans interruption.",
                            "children": [
                                {
                                    "label": "Cyclique",
                                    "group": 0x3A7945,
                                    "rang": 5,
                                    "posx": 1265,
                                    "posy": -495,
                                    "posz": -690,
                                    "synonymes": ["Cyclique", "Répétition", "Révolution"],
                                    "définition": "Qui se reproduit selon un cycle, à intervalles réguliers",
                                    "children": [
                                        {
                                            "label": "Périodique",
                                            "group": 0x3A7945,
                                            "rang": 6,
                                            "posx": 1700,
                                            "posy": -275,
                                            "posz": -700,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Périodique</b><br/>Mouvement des planètes<br/>Catastrophes géologiques<br/><b>Apériodique</b>",
                                            "définition": "Qui a lieu, qui se reproduit à intervalles généralement réguliers, à des moments déterminés."
                                        },
                                        {
                                            "label": "Apériodique",
                                            "group": 0x3A7945,
                                            "rang": 6,
                                            "posx": 1600,
                                            "posy": -575,
                                            "posz": -700,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Périodique</b><br/>Mouvement des planètes<br/>Catastrophes géologiques<br/><b>Apériodique</b>",
                                            "définition": "Phénomène qui se répète, sans avoir de période."
                                        }
                                    ]

                                },
                                {
                                    "label": "Linéaire",
                                    "group": 0x3A7945,
                                    "rang": 5,
                                    "posx": 1145,
                                    "posy": -250,
                                    "posz": -945,
                                    "définition": "Qui a l'aspect continu d'une ligne, qui se traduit par des lignes : Représentation linéaire du temps.",
                                    "children": [
                                        {
                                            "label": "Erosion",
                                            "group": 0x3A7945,
                                            "rang": 6,
                                            "posx": 1160,
                                            "posy": -275,
                                            "posz": -1450,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Erosion</b><br/>Réseaux hygrographique<br/>Géologie<br/>Granulométrie<br/><b>Sédimentation</b>",
                                            "définition": "Action d'un agent, d'une substance qui ronge, use progressivement; résultat de cette action."
                                        },
                                        {
                                            "label": "Sédimentation",
                                            "group": 0x3A7945,
                                            "rang": 6,
                                            "posx": 1245,
                                            "posy": -610,
                                            "posz": -1250,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Erosion</b><br/>Réseaux hygrographique<br/>Géologie<br/>Granulométrie<br/><b>Sédimentation</b>",
                                            "définition": "Ensemble des processus qui aboutissent à la formation des sédiments. Mode de sédimentation; milieu, bassin, fosse de sédimentation."
                                        }
                                    ]
                                }
                            ]
            },
                        {
                            "label": "Discontinuité",
                            "group": 0x3A7945,
                            "rang": 4,
                            "posx": 810,
                            "posy": 240,
                            "posz": -370,
                            "synonymes": ["Discontinuité", "Intermittence", "interruption"],
                            "définition": "Absence de continuité dans l'espace ou dans le temps. ",
                            "children": [
                                {
                                    "label": "Graduel",
                                    "group": 0x3A7945,
                                    "rang": 5,
                                    "posx": 1145,
                                    "posy": 230,
                                    "posz": -945,
                                    "synonymes": ["Graduel", "Affaiblissement", "accumulation", "ascension", "développement", "perfectionnement", "graduel"],
                                    "définition": "Qui progresse par degrés, par étapes ",
                                    "children": [
                                        {
                                            "label": "Marque",
                                            "group": 0x3A7945,
                                            "rang": 6,
                                            "posx": 1200,
                                            "posy": 260,
                                            "posz": -1450,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Marque</b><br/>Mont<br/>Plaine<br/>Fosse<br/><b>Rifting</b>",
                                            "synonymes": ["Marque", "Signe", "Empreinte"],
                                            "définition": "Traces de morphologie géographique."
                                        },
                                        {
                                            "label": "Rifting",
                                            "group": 0x3A7945,
                                            "rang": 6,
                                            "posx": 1300,
                                            "posy": 580,
                                            "posz": -1250,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Marque</b><br/>Mont<br/>Plaine<br/>Fosse<br/><b>Rifting</b>",
                                            "définition": "Fossé provoqué par un effondrement."
                                        }
                                    ]

                                },
                                {
                                    "label": "Brutal",
                                    "group": 0x3A7945,
                                    "rang": 5,
                                    "posx": 1265,
                                    "posy": 435,
                                    "posz": -690,
                                    "synonymes": ["Brutal", "brutal"],
                                    "définition": "Changement de direction radicale ",
                                    "children": [
                                        {
                                            "label": "Rupture",
                                            "group": 0x3A7945,
                                            "rang": 6,
                                            "posx": 1700,
                                            "posy": 430,
                                            "posz": -710,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Rupture</b><br/>Rupture rocheuse<br/>Catastrophe climatique<br/><b>Destruction</b>",
                                            "synonymes": ["Rupture", "Désunion", "cassure"],
                                            "définition": "Fracture d'une chose solide en deux ou plusieurs parties sous l'effet d'efforts ou de contraintes trop intenses."
                                        },
                                        {
                                            "label": "Destruction",
                                            "group": 0x3A7945,
                                            "rang": 6,
                                            "posx": 1600,
                                            "posy": 730,
                                            "posz": -710,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Rupture</b><br/>Rupture rocheuse<br/>Catastrophe climatique<br/><b>Destruction</b>",
                                            "synonymes": ["Destruction"],
                                            "définition": " Action de faire disparaître complètement et résultat de cette action."
                                        }
                                    ]
                                }
                            ]
            }
          ]
        }
      ]
    },
        {
            "label": "Formes Anthropologiques",
            "group": 0x3A4779,
            "rang": 2,
            "posx": -140,
            "posy": -175,
            "posz": 125,
            "synonymes": ["Formes Anthropologiques"],
            "définition": "Relate l'histoire et l'évolution de l'être humain.",
            "children": [
                {
                    "label": "Jupiter",
                    "group": 0x3A4779,
                    "rang": 3,
                    "posx": -345,
                    "posy": -250,
                    "posz": 230,
                    "synonymes": ["Jupiter"],
                    "définition": "Intelligentsia - La souveraineté spirituelle et intellectuelle qui fonde les valeurs de la société",
                    "children": [
                        {
                            "label": "Sacré",
                            "group": 0x3A4779,
                            "rang": 4,
                            "posx": -480,
                            "posy": -390,
                            "posz": 685,
                            "synonymes": ["Sacré", "Saint", "intangible", "religieux"],
                            "définition": "Qui appartient à un domaine séparé, inviolable, privilégié par son rapport au divin.",
                            "children": [
                                {
                                    "label": "Croyance",
                                    "group": 0x3A4779,
                                    "rang": 5,
                                    "posx": -950,
                                    "posy": -430,
                                    "posz": 1080,
                                    "synonymes": ["Croyance", "Certitude", "conviction", "foi"],
                                    "définition": "Conviction personnelle d'une existence du sacré",
                                    "children": [
                                        {
                                            "label": "Mythes",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -1080,
                                            "posy": -520,
                                            "posz": 1420,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Mythe</b><br/>Eschatologie<br/>Cosmogonie<br/>Appartenance religieuse<br/>Prosélythisme<br/><b>Dogme</b>",
                                            "synonymes": ["Mythes",
                                            "Fable", "légende", "allégorie"],
                                            "définition": "Récit fondateur relatant des faits imaginaires non consignés par l'histoire, transmis par la tradition."
                                        },
                                        {
                                            "label": "Dogmes",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -1010,
                                            "posy": -70,
                                            "posz": 1580,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Mythe</b><br/>Eschatologie<br/>Cosmogonie<br/>Appartenance religieuse<br/>Prosélythisme<br/><b>Dogme</b>",
                                            "synonymes": ["Dogmes", "Article", "croyance", "doctrine"],
                                            "définition": "Proposition théorique établie comme vérité indiscutable par l'autorité qui régit une certaine communauté."
                                        }
                                    ]

                                },
                                {
                                    "label": "Adoration",
                                    "group": 0x3A4779,
                                    "rang": 5,
                                    "posx": -600,
                                    "posy": -665,
                                    "posz": 1180,
                                    "synonymes": ["Adoration", "Culte", "latrie"],
                                    "définition": "Acte de dévotion affirmant sa croyance au sacré",
                                    "children": [
                                        {
                                            "label": "Culte",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -720,
                                            "posy": -740,
                                            "posz": 1560,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Culte</b><br/>Lieux de pratique<br/>Regroupement communautaire<br/>Institution<br/>Initiation<br/>Célébration<br/><b>Rite</b>",
                                            "synonymes": ["Cultes", "Dévouement", "vénération"],
                                            "définition": "Hommage qu'on rend à la divinité."
                                        },
                                        {
                                            "label": "Rite",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -460,
                                            "posy": -460,
                                            "posz": 1760,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Culte</b><br/>Lieux de pratique<br/>Regroupement communautaire<br/>Institution<br/>Initiation<br/>Célébration<br/><b>Rite</b>",
                                            "synonymes": ["Rites", "Culte", "liturgie"],
                                            "définition": "Pratiques régulières d'adoration"
                                        }
                                    ]
                                }
                            ]
            },
                        {
                            "label": "Profane",
                            "group": 0x3A4779,
                            "rang": 4,
                            "posx": -750,
                            "posy": -175,
                            "posz": 500,
                            "synonymes": ["Profane", "Ignorant", "séculier", "laïc"],
                            "définition": "Qui n'appartient pas à la religion, qui ne fait pas appel à une entitié supérieure.",
                            "children": [
                                {
                                    "label": "Hypothèse",
                                    "group": 0x3A4779,
                                    "rang": 5,
                                    "posx": -1445,
                                    "posy": -135,
                                    "posz": 410,
                                    "synonymes": ["Hypothèse", "supposition"],
                                    "définition": "Supposition d'une chose possible ou non de laquelle on tire une conséquence.",
                                    "children": [
                                        {
                                            "label": "Conjecture",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -1800,
                                            "posy": -330,
                                            "posz": 390,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Conjecture</b><br/>Superstitions<br/>Divinations<br/>Spiritisme<br/>Médecine occulte<br/>Alchimie<br/><b>Experimentation</b>",
                                            "synonymes": ["Conjecture", "Circonstance", "cas", "état", "occurence", "situation"],
                                            "définition": "Idée non vérifiée, fondée soit sur une probabilité, soit sur l'apparence."
                                        },
                                        {
                                            "label": "Expérimentation",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -1835,
                                            "posy": 120,
                                            "posz": 390,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Conjecture</b><br/>Superstitions<br/>Divinations<br/>Spiritisme<br/>Médecine occulte<br/>Alchimie<br/><b>Experimentation</b>",
                                            "synonymes": ["Expérimentation"],
                                            "définition": "Méthode basée sur la pratique permettant de vérifier les hypothèses avancées."
                                        }
                                    ]

                                },
                                {
                                    "label": "Doctrine",
                                    "group": 0x3A4779,
                                    "rang": 5,
                                    "posx": -1300,
                                    "posy": -230,
                                    "posz": 750,
                                    "synonymes": ["Doctrine"],
                                    "définition": "Ensemble des savoirs fondamentaux",
                                    "children": [
                                        {
                                            "label": "Archive",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -1600,
                                            "posy": -365,
                                            "posz": 900,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Archive</b><br/>Lieux géographiques<br/>Lieux génériques<br/>Lieux particuliers<br/>Patrimoine artistique<br/>Graphie<br/>Savoir-faire<br/>Associations<br/>Communication orale<br/>Education/Apprentissage<br/><b>Diffusion</b>",
                                            "synonymes": ["Archive", "Bibliothèque", "dépôt"],
                                            "définition": "Conservation de connaissances"
                                        },
                                        {
                                            "label": "Diffusion",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -1635,
                                            "posy": 60,
                                            "posz": 900,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Archive</b><br/>Lieux géographiques<br/>Lieux génériques<br/>Lieux particuliers<br/>Patrimoine artistique<br/>Graphie<br/>Savoir-faire<br/>Associations<br/>Communication orale<br/>Education/Apprentissage<br/><b>Diffusion</b>",
                                            "synonymes": ["Diffusion"],
                                            "définition": "Transmission de connaissances"
                                        }
                                    ]
                                }
                            ]
            }
          ]
        },
                {
                    "label": "Mars",
                    "group": 0x3A4779,
                    "rang": 3,
                    "posx": -140,
                    "posy": -440,
                    "posz": 185,
                    "synonymes": ["Mars"],
                    "children": [
                        {
                            "label": "Civil",
                            "group": 0x3A4779,
                            "rang": 4,
                            "posx": -10,
                            "posy": -890,
                            "posz": 230,
                            "synonymes": ["Civil", "Bourgeois", "laïc", "poli"],
                            "définition": "Stabilisation de la société par une force interne à celle-ci. Il s'agit d'une force cohésive interne.",
                            "children": [
                                {
                                    "label": "Souveraineté",
                                    "group": 0x3A4779,
                                    "rang": 5,
                                    "posx": -25,
                                    "posy": -1445,
                                    "posz": 430,
                                    "synonymes": ["Souveraineté", "Empire", "pouvoir", "indépendance"],
                                    "définition": "Autorité d'une personne ou d'un groupe de personnes (Etat) sur un peuple. Son action s'applique aux échelles nationales et internationales.",
                                    "children": [
                                        {
                                            "label": "Nomination",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": 240,
                                            "posy": -1800,
                                            "posz": 440,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Nomination</b><br/>Hasard<br/>Cooptation<br/>Règles<br/>Symboles d'autorité<br/>Lieux d'autorité<br/><b>Représentation</b>",
                                            "synonymes": ["Nomination"],
                                            "définition": "Une personne devient chef par des voies naturelles sans élection. Il est nommé par hérédité ou par un autre entité."
                                        },
                                        {
                                            "label": "Représentation",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -240,
                                            "posy": -1800,
                                            "posz": 440,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Nomination</b><br/>Hasard<br/>Cooptation<br/>Règles<br/>Symboles d'autorité<br/>Lieux d'autorité<br/><b>Représentation</b>",
                                            "synonymes": ["Représentation", "Vrai", "Tangible", "Réel", "vrai", "tangible", "réel", "reel"],
                                            "définition": "Le chef est élu par une autre entité qu'il doit représenter"
                                        }
                                    ]

                                },
                                {
                                    "label": "Allégeance",
                                    "group": 0x3A4779,
                                    "rang": 5,
                                    "posx": -75,
                                    "posy": -1490,
                                    "posz": -110,
                                    "synonymes": ["Allégeance"],
                                    "children": [
                                        {
                                            "label": "Alliance",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": 195,
                                            "posy": -1825,
                                            "posz": -335,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Alliance</b><br/>Alliance mentale<br/>Alliance physique<br/>Alliance financière<br/>Scission mentale<br/>Scission physique<br/>Scission financière<br/><b>Scission</b>",
                                            "synonymes": ["Alliance", "Vrai", "Tangible", "Réel", "vrai", "tangible", "réel", "reel"],
                                            "définition": "Union entre des personnes ou des collectivités que rapproche une communauté de sentiments, d'idées, d'intérêts"
                                        },
                                        {
                                            "label": "Scission",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -225,
                                            "posy": -1825,
                                            "posz": -245,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Alliance</b><br/>Alliance mentale<br/>Alliance physique<br/>Alliance financière<br/>Scission mentale<br/>Scission physique<br/>Scission financière<br/><b>Scission</b>",
                                            "synonymes": ["Scission"],
                                            "définition": "Séparation volontaire et/ou ordonnée d'un tout en ses éléments"
                                        }
                                    ]
                                }
                            ]
            },
                        {
                            "label": "Militaire",
                            "group": 0x3A4779,
                            "rang": 4,
                            "posx": -50,
                            "posy": -750,
                            "posz": 510,
                            "synonymes": ["Militaire", "Martial", "guerrier"],
                            "définition": "Stabilisation de la société par une force extérieure. Il s'agit d'une force physique dominante.",
                            "children": [
                                {
                                    "label": "Défensive",
                                    "group": 0x3A4779,
                                    "rang": 5,
                                    "posx": -145,
                                    "posy": -970,
                                    "posz": 1150,
                                    "synonymes": ["Défensive", "Martial", "guerrier"],
                                    "définition": "Défense - ce qui cherche la staticité, l'équilibre après une agression.",
                                    "children": [
                                        {
                                            "label": "Attente",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -190,
                                            "posy": -1075,
                                            "posz": 1520,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Attente</b><br/>Surveillance<br/>Préparation<br/>Repli<br/>Résistance<br/><b>Parade</b>",
                                            "synonymes": ["Attente"],
                                            "définition": "Etat expectatif d'un coup de l'ennemi"
                                        },
                                        {
                                            "label": "Parade",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": 250,
                                            "posy": -900,
                                            "posz": 1615,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Attente</b><br/>Surveillance<br/>Préparation<br/>Repli<br/>Résistance<br/><b>Parade</b>",
                                            "synonymes": ["Parade"],
                                            "définition": "Bloquer les coups de l'ennemi. Faire bouclier."
                                        }
                                    ]

                                },
                                {
                                    "label": "Offensive",
                                    "group": 0x3A4779,
                                    "rang": 5,
                                    "posx": 5,
                                    "posy": -1220,
                                    "posz": 890,
                                    "synonymes": ["Offensive"],
                                    "définition": "Attaque - ce qui cherche le changement, la rupture en imposant à l'ennemi sa volonté.",
                                    "children": [
                                        {
                                            "label": "Agrégation",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -225,
                                            "posy": -1525,
                                            "posz": 1075,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Agrégation</b><br/>Attaque<br/>Confrontation<br/>Envahissement<br/>Territoire d'occupation<br/>Trajectoire militaire<br/>Stratégie<br/>Tension<br/>Emprisonnement<br/>Crime<br/><b>Fragmentation</b>",
                                            "synonymes": ["Agrégation"]
                                        },
                                        {
                                            "label": "Fragmentation",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": 240,
                                            "posy": -1470,
                                            "posz": 1130,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Agrégation</b><br/>Attaque<br/>Confrontation<br/>Envahissement<br/>Territoire d'occupation<br/>Trajectoire militaire<br/>Stratégie<br/>Tension<br/>Emprisonnement<br/>Crime<br/><b>Fragmentation</b>",
                                            "synonymes": ["Fragmentation", "désagrégation"]
                                        }
                                    ]
                                }
                            ]
            }
          ]
        },
                {
                    "label": "Quirinus",
                    "group": 0x3A4779,
                    "rang": 3,
                    "posx": -320,
                    "posy": -375,
                    "posz": 0,
                    "synonymes": ["Quirinus"],
                    "définition": "Aptitude à se reproduire",
                    "children": [
                        {
                            "label": "Fertilité",
                            "group": 0x3A4779,
                            "rang": 4,
                            "posx": -420,
                            "posy": -750,
                            "posz": -330,
                            "synonymes": ["Fertilité"],
                            "définition": "Capacité de créer, produire, inventer",
                            "children": [
                                {
                                    "label": "Ressource",
                                    "group": 0x3A4779,
                                    "rang": 5,
                                    "posx": -820,
                                    "posy": -1050,
                                    "posz": -680,
                                    "synonymes": ["Ressource", "Richesse"],
                                    "définition": "Moyens matériels et immatériels dont dispose un pays, une région, une collectivité",
                                    "children": [
                                        {
                                            "label": "Energie",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -1000,
                                            "posy": -1230,
                                            "posz": -990,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Energie</b><br/>Force de travail<br/>Energie renouvelable<br/>Fossile<br/>Biomasses<br/>Consommable<br/><b>Matière</b>",
                                            "synonymes": ["Energie", "énergétisme", "fortifiant"],
                                            "définition": "Capacité d'un corps ou d'un système à produire du travail mécanique."
                                        },
                                        {
                                            "label": "Matière",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -1100,
                                            "posy": -1370,
                                            "posz": -620,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Energie</b><br/>Force de travail<br/>Energie renouvelable<br/>Fossile<br/>Biomasses<br/>Consommable<br/><b>Matière</b>",
                                            "synonymes": ["Matière", "Energie", "énergétisme", "fortifiant"],
                                            "définition": "Substance constituant les corps."
                                        }
                                    ]

                                },
                                {
                                    "label": "Production",
                                    "group": 0x3A4779,
                                    "rang": 5,
                                    "posx": -530,
                                    "posy": -1320,
                                    "posz": -500,
                                    "synonymes": ["Production", "Ouvrage", "produit", "élaboration"],
                                    "définition": "Ensemble des actions qui permettent d'engendrer, de créer.",
                                    "children": [
                                        {
                                            "label": "Conception",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -600,
                                            "posy": -1700,
                                            "posz": -490,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Conception</b><br/>Extraction<br/>Reproduction<br/>Transformation<br/>Moral<br/>Physique<br/><b>Concrétisation</b>",
                                            "synonymes": ["Conception"],
                                            "définition": "Formation d'une idée générale, se représenter un objet par la pensée."
                                        },
                                        {
                                            "label": "Concrétisation",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -440,
                                            "posy": -1570,
                                            "posz": -930,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Conception</b><br/>Extraction<br/>Reproduction<br/>Transformation<br/>Moral<br/>Physique<br/><b>Concrétisation</b>",
                                            "synonymes": ["Concrétisation"],
                                            "définition": "Matérialiser une idée."
                                        }
                                    ]
                                }
                            ]
            },
                        {
                            "label": "Prospérité",
                            "group": 0x3A4779,
                            "rang": 4,
                            "posx": -730,
                            "posy": -460,
                            "posz": -330,
                            "synonymes": ["Prospérité"],
                            "définition": "État heureux, situation favorable, conséquence d'une production généreuse.",
                            "children": [
                                {
                                    "label": "Expansion",
                                    "group": 0x3A4779,
                                    "rang": 5,
                                    "posx": -1400,
                                    "posy": -460,
                                    "posz": -220,
                                    "synonymes": ["Expansion"],
                                    "définition": "Développement de produits dans une limite nécessaire.",
                                    "children": [
                                        {
                                            "label": "Aménagement",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -1765,
                                            "posy": -540,
                                            "posz": -240,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Aménagement</b><br/>Stockage<br/>Fret<br/>Vente<br/>Troc<br/><b>Echange</b>",
                                            "synonymes": ["Aménagement"],
                                            "définition": "Action d'adapter, modifier quelque chose de manière à le rendre plus adéquat."
                                        },
                                        {
                                            "label": "Echange",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -1800,
                                            "posy": -220,
                                            "posz": -435,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Aménagement</b><br/>Stockage<br/>Fret<br/>Vente<br/>Troc<br/><b>Echange</b>",
                                            "synonymes": ["Echange", "Perméabilité", "contrepartie", "retour"],
                                            "définition": "Action ou fait de donner une chose et d'en recevoir une autre en contrepartie"
                                        }
                                    ]

                                },
                                {
                                    "label": "Profusion",
                                    "group": 0x3A4779,
                                    "rang": 5,
                                    "posx": -1200,
                                    "posy": -730,
                                    "posz": -550,
                                    "synonymes": ["Profusion"],
                                    "définition": "Développement excessive d'un produit. Abondance extrême.",
                                    "children": [
                                        {
                                            "label": "Accumulation",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -1480,
                                            "posy": -980,
                                            "posz": -560,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Accumulation</b><br/>Capitalisation<br/>Héritage<br/>Envie<br/>Mode<br/>Jalousie<br/><b>Désir</b>",
                                            "synonymes": ["Accumulation"],
                                            "définition": "Action de réunir progressivement un ensemble important d'éléments."
                                        },
                                        {
                                            "label": "Désir",
                                            "group": 0x3A4779,
                                            "rang": 6,
                                            "posx": -1450,
                                            "posy": -775,
                                            "posz": -870,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Accumulation</b><br/>Capitalisation<br/>Héritage<br/>Envie<br/>Mode<br/>Jalousie<br/><b>Désir</b>",
                                            "synonymes": ["Désir", "Appétance", "aspiration", "attrait", "inclination", "souhait"],
                                            "définition": "Aspiration instinctive de l'être à combler le sentiment d'un manque."
                                        }
                                    ]
                                }
                            ]
            }
          ]
        }
      ]
    },
        {
            "label": "Formes Culturelles",
            "group": 0x952222,
            "rang": 2,
            "posx": -150,
            "posy": 200,
            "posz": -110,
            "synonymes": ["Formes Culturelles", "Instructif", "éducatif", "ethnique"],
            "définition": "Décrit la société.",
            "children": [
                {
                    "label": "Normes",
                    "group": 0x952222,
                    "rang": 3,
                    "posx": -390,
                    "posy": 285,
                    "posz": -110,
                    "synonymes": ["Normes", "Canon", "idéal", "loi", "modèle"],
                    "définition": "Principes adoptées par le groupe",
                    "children": [
                        {
                            "label": "Moeurs",
                            "group": 0x952222,
                            "rang": 4,
                            "posx": -715,
                            "posy": 570,
                            "posz": 115,
                            "synonymes": ["Moeurs", "Conduite", "morale", "principes"],
                            "définition": "Valeurs morales convergentes ou divergentes",
                            "children": [
                                {
                                    "label": "Contestation",
                                    "group": 0x952222,
                                    "rang": 5,
                                    "posx": -1100,
                                    "posy": 740,
                                    "posz": 690,
                                    "synonymes": ["Contestation", "Dénégation", "débat", "objection"],
                                    "définition": "Refus de la norme",
                                    "children": [
                                        {
                                            "label": "Marginalité",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -1470,
                                            "posy": 625,
                                            "posz": 965,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Marginalité</b><br/>Volontaire<br/>Imposé<br/>Identitaire<br/><b>Militance</b>",
                                            "synonymes": ["Marginalité", "Associal", "Anti-conformisme"],
                                            "définition": "Situation d'exclusion de la norme"
                                        },
                                        {
                                            "label": "Militance",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -1650,
                                            "posy": 735,
                                            "posz": 465,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Marginalité</b><br/>Volontaire<br/>Imposé<br/>Identitaire<br/><b>Militance</b>",
                                            "synonymes": ["Militance", "Engagement, militantisme"],
                                            "définition": "Lutte pour modifier la norme"
                                        }
                                    ]

                                },
                                {
                                    "label": "Conformisme",
                                    "group": 0x952222,
                                    "rang": 5,
                                    "posx": -790,
                                    "posy": 1040,
                                    "posz": 690,
                                    "synonymes": ["Conformisme", "Orthodoxie", "Mimétisme", "Suivisme"],
                                    "définition": "Soumission générale  à la norme",
                                    "children": [
                                        {
                                            "label": "Particularisme",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -1050,
                                            "posy": 1460,
                                            "posz": 495,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Particularisme</b><br/>Coutume<br/>Ecriture<br/><b>Oeucuménique</b>",
                                            "synonymes": ["Particularisme"],
                                            "définition": "Renforcement local de la norme"
                                        },
                                        {
                                            "label": "Oeucuménique",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -965,
                                            "posy": 1290,
                                            "posz": 990,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Particularisme</b><br/>Coutume<br/>Ecriture<br/><b>Oeucuménique</b>",
                                            "synonymes": ["Oeucuménique", "Général", "Universel", "Mondial", "Planétaire"],
                                            "définition": "Renforcement global de la norme"
                                        }
                                    ]
                                }
                            ]
            },
                        {
                            "label": "Préceptes",
                            "group": 0x952222,
                            "rang": 4,
                            "posx": -750,
                            "posy": 450,
                            "posz": -250,
                            "synonymes": ["Préceptes", "Leçon", "maxime", "prescription"],
                            "définition": "Institution de règles",
                            "children": [
                                {
                                    "label": "Jurisprudence",
                                    "group": 0x952222,
                                    "rang": 5,
                                    "posx": -1040,
                                    "posy": 970,
                                    "posz": -440,
                                    "synonymes": ["Jurisprudence", "Coutume", "doctrine"],
                                    "définition": "Modification de la norme à l'épreuve des faits",
                                    "children": [
                                        {
                                            "label": "Hérésie",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -1390,
                                            "posy": 1225,
                                            "posz": -225,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Hérésie</b><br/>Elaboration et diffusion<br/>Crises<br/>Refus ou acceptation<br/><b>Réforme</b>",
                                            "synonymes": ["Hérésie", "Sacrilège"],
                                            "définition": "Modification condamnée de la norme"
                                        },
                                        {
                                            "label": "Réforme",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -1350,
                                            "posy": 1145,
                                            "posz": -620,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Hérésie</b><br/>Elaboration et diffusion<br/>Crises<br/>Refus ou acceptation<br/><b>Réforme</b>",
                                            "synonymes": ["Réforme", "Révision"],
                                            "définition": "Modification acceptée de la norme"
                                        }
                                    ]

                                },
                                {
                                    "label": "Codes",
                                    "group": 0x952222,
                                    "rang": 5,
                                    "posx": -1290,
                                    "posy": 550,
                                    "posz": -565,
                                    "synonymes": ["Codes", "Réglement"],
                                    "définition": "Entérinement de la norme",
                                    "children": [
                                        {
                                            "label": "Règle",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -1615,
                                            "posy": 665,
                                            "posz": -675,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Règles</b><br/>Droit coutumier<br/>Règlement<br/>Ordonnances<br/>Codes<br/>Lois ordinaires<br/>Lois de finances<br/>Lois organiques<br/>Lois constitutionnelles<br/>Constitution<br/><b>Loi</b>",
                                            "synonymes": ["Règle", "Convention", "code", "méthode", "discipline", "usage"],
                                            "définition": "Reconnaissance de principes normatifs"
                                        },
                                        {
                                            "label": "Loi",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -1740,
                                            "posy": 670,
                                            "posz": -235,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Règles</b><br/>Droit coutumier<br/>Règlement<br/>Ordonnances<br/>Codes<br/>Lois ordinaires<br/>Lois de finances<br/>Lois organiques<br/>Lois constitutionnelles<br/>Constitution<br/><b>Loi</b>",
                                            "synonymes": ["Loi", "Droit", "licite"],
                                            "définition": "Inscriptions et pérennisation de principes normatifs"
                                        }
                                    ]
                                }
                            ]
            }
          ]
        },
                {
                    "label": "Institutions",
                    "group": 0x952222,
                    "rang": 3,
                    "posx": -90,
                    "posy": 470,
                    "posz": -95,
                    "synonymes": ["Institutions", "Constitution", "régime"],
                    "définition": "Outils forgés par le groupe",
                    "children": [
                        {
                            "label": "Etat",
                            "group": 0x952222,
                            "rang": 4,
                            "posx": -130,
                            "posy": 900,
                            "posz": 115,
                            "synonymes": ["Etat", "Administration", "gouvernement", "pouvoir"],
                            "définition": "Gestion et/ou contrôle de la structure sociale ",
                            "children": [
                                {
                                    "label": "Legs",
                                    "group": 0x952222,
                                    "rang": 5,
                                    "posx": 185,
                                    "posy": 1260,
                                    "posz": 750,
                                    "synonymes": ["Legs", "Héritage"],
                                    "définition": "Projection historique de la société",
                                    "children": [
                                        {
                                            "label": "Politique",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": 270,
                                            "posy": 1465,
                                            "posz": 1120,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Politique</b><br/>Représentation politique<br/>Citoyenneté<br/>Services publics<br/>Collectivités territoriales<br/><b>Administration</b>",
                                            "synonymes": ["Politique"],
                                            "définition": "Construction de la gouvernance sociétale"
                                        },
                                        {
                                            "label": "Administration",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": 450,
                                            "posy": 1645,
                                            "posz": 745,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Politique</b><br/>Représentation politique<br/>Citoyenneté<br/>Services publics<br/>Collectivités territoriales<br/><b>Administration</b>",
                                            "synonymes": ["Administration", "Gestion"],
                                            "définition": "Fonctionnement de la gouvernance sociétale"
                                        }
                                    ]

                                },
                                {
                                    "label": "Pari",
                                    "group": 0x952222,
                                    "rang": 5,
                                    "posx": -140,
                                    "posy": 1425,
                                    "posz": 390,
                                    "synonymes": ["Filiation", "Enchaînement", "liaison"],
                                    "children": [
                                        {
                                            "label": "Recherche",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -5,
                                            "posy": 1800,
                                            "posz": 520,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Recherche</b><br/>Recherche fondamentales<br/>Recherche stratégique<br/>Recherche appliquée<br/>Planification stratégique<br/>Planification tactique<br/>Planification opérationnelle<br/><b>Planification</b>",
                                            "synonymes": ["Recherche", "Spéculation", "expérimentation", "Investigation"],
                                            "définition": "Investigations sur le devenir d'une société"
                                        },
                                        {
                                            "label": "Planification",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -300,
                                            "posy": 1830,
                                            "posz": 250,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Recherche</b><br/>Recherche fondamentales<br/>Recherche stratégique<br/>Recherche appliquée<br/>Planification stratégique<br/>Planification tactique<br/>Planification opérationnelle<br/><b>Planification</b>",
                                            "synonymes": ["Planification"],
                                            "définition": "Organisation du devenir d'une société"
                                        }
                                    ]
                                }
                            ]
            },
                        {
                            "label": "Société",
                            "group": 0x952222,
                            "rang": 4,
                            "posx": 50,
                            "posy": 890,
                            "posz": -250,
                            "synonymes": ["Société"],
                            "définition": "Structure sociale dans son ensemble",
                            "children": [
                                {
                                    "label": "Filiation",
                                    "group": 0x952222,
                                    "rang": 5,
                                    "posx": 430,
                                    "posy": 1400,
                                    "posz": -250,
                                    "synonymes": ["Filiation"],
                                    "définition": "Le fait, pour le corps social, d'agir dans la continuation de préceptes hérités du sous-groupe d'appartenance.",
                                    "children": [
                                        {
                                            "label": "Conservatisme",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": 880,
                                            "posy": 1635,
                                            "posz": -250,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Conservatisme</b><br/>Sauvegarde<br/>Enseignement<br/>Religion<br/>Mobilité<br/>Immobilité<br/>Mixité<br/><b>Métissage</b>",
                                            "synonymes": ["Conservatisme", "traditionalisme"],
                                            "définition": "Fixation des caractères d'une société"
                                        },
                                        {
                                            "label": "Métissage",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": 420,
                                            "posy": 1810,
                                            "posz": -250,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Conservatisme</b><br/>Sauvegarde<br/>Enseignement<br/>Religion<br/>Mobilité<br/>Immobilité<br/>Mixité<br/><b>Métissage</b>",
                                            "synonymes": ["Métissage", "Hybridation"],
                                            "définition": "Croisements culturels au sein d'une société"
                                        }
                                    ]

                                },
                                {
                                    "label": "Adhésion",
                                    "group": 0x952222,
                                    "rang": 5,
                                    "posx": -110,
                                    "posy": 1465,
                                    "posz": -250,
                                    "synonymes": ["Adhésion", "Affiliation", "ralliement"],
                                    "définition": "Le fait, pour le corps social, d'entreprendre des activités de manière spontanée et enthousiaste.",
                                    "children": [
                                        {
                                            "label": "Animation",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -270,
                                            "posy": 1850,
                                            "posz": -250,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Animation</b><br/>Engagement<br/>Affiliation numérique<br/>Loisirs<br/>Spectacle vivant<br/>Spectacle enregistré<br/>Subventionnement<br/>Encouragement<br/><b>Liker</b>",
                                            "synonymes": ["Animation", "Entrain", "activité", "mouvement"],
                                            "définition": "Conduite d'une dynamique sociétale"
                                        },
                                        {
                                            "label": "Liker",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -740,
                                            "posy": 1705,
                                            "posz": -250,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Animation</b><br/>Engagement<br/>Affiliation numérique<br/>Loisirs<br/>Spectacle vivant<br/>Spectacle enregistré<br/>Subventionnement<br/>Encouragement<br/><b>Liker</b>",
                                            "synonymes": ["Liker"],
                                            "définition": "Etre en accord avec une dynamique sociétale"
                                        }
                                    ]
                                }
                            ]
            }
          ]
        },
                {
                    "label": "Artefacts",
                    "group": 0x952222,
                    "rang": 3,
                    "posx": -150,
                    "posy": 300,
                    "posz": -350,
                    "synonymes": ["Artefacts"],
                    "définition": "L'ensemble des formes ou structures sociales, telles qu'elles sont établies par la loi ou la coutume, et spécialement celles qui relèvent du droit public.",
                    "children": [
                        {
                            "label": "Information",
                            "group": 0x952222,
                            "rang": 4,
                            "posx": -380,
                            "posy": 420,
                            "posz": -725,
                            "synonymes": ["Information", "Message", "cybernétique", "donnée"],
                            "définition": "Distribution de données",
                            "children": [
                                {
                                    "label": "Art",
                                    "group": 0x952222,
                                    "rang": 5,
                                    "posx": -715,
                                    "posy": 930,
                                    "posz": -950,
                                    "synonymes": ["Art"],
                                    "définition": "Emploi d'artefacts non-utilitaires",
                                    "children": [
                                        {
                                            "label": "Tendances",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -660,
                                            "posy": 1480,
                                            "posz": -950,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Tendances</b><br/>Création<br/>Diffusion<br/>Soutien<br/>Exposition<br/>Référence<br/><b>Mouvements</b>",
                                            "synonymes": ["Tendances", "Courant", "mouvance", "inclination", "penchant"],
                                            "définition": "L'ensemble de la pensée et des actions novatrices en matière d'Art, qui tendent à rompre avec la tradition."
                                        },
                                        {
                                            "label": "Mouvements",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -1060,
                                            "posy": 1240,
                                            "posz": -950,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Tendances</b><br/>Création<br/>Diffusion<br/>Soutien<br/>Exposition<br/>Référence<br/><b>Mouvements</b>",
                                            "synonymes": ["Mouvements"]
                                        }
                                    ]

                                },
                                {
                                    "label": "Equipement",
                                    "group": 0x952222,
                                    "rang": 5,
                                    "posx": -700,
                                    "posy": 565,
                                    "posz": -1205,
                                    "synonymes": ["Equipement", "Aménagement", "outillage"],
                                    "définition": "Emploi d'artefacts utilitaires",
                                    "children": [
                                        {
                                            "label": "Modes",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -840,
                                            "posy": 350,
                                            "posz": -1650,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Modes</b><br/>Art de vivre<br/>Vestimentaire<br/>Loisirs<br/>Gastronomie<br/>Ondes<br/>Aérien<br/>Maritime<br/>Terrestre<br/><b>Edification</b>",
                                            "synonymes": ["Modes", "Vogue"]
                                        },
                                        {
                                            "label": "Edifications",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -525,
                                            "posy": 750,
                                            "posz": -1650,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Modes</b><br/>Art de vivre<br/>Vestimentaire<br/>Loisirs<br/>Gastronomie<br/>Ondes<br/>Aérien<br/>Maritime<br/>Terrestre<br/><b>Edification</b>",
                                            "synonymes": ["Edifications", "Création", "constitution"]
                                        }
                                    ]
                                }
                            ]
            },
                        {
                            "label": "Format",
                            "group": 0x952222,
                            "rang": 4,
                            "posx": -30,
                            "posy": 575,
                            "posz": -725,
                            "synonymes": ["Format", "Calibre"],
                            "définition": "Fixation de processus reproductibles",
                            "children": [
                                {
                                    "label": "Prototype",
                                    "group": 0x952222,
                                    "rang": 5,
                                    "posx": 315,
                                    "posy": 815,
                                    "posz": -1230,
                                    "synonymes": ["Prototype", "Archétype", "modèle"],
                                    "définition": "Nouvel artefact",
                                    "children": [
                                        {
                                            "label": "Sur-mesure",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -15,
                                            "posy": 880,
                                            "posz": -1665,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Sur-mesure</b><br/>Textile<br/>Architecture<br/>Design<br/>Brevetage<br/>Norme d'applications<br/>Organisme<br/>Teste<br/><b>Etalonnage</b>",
                                            "synonymes": ["Sur-mesure"],
                                            "définition": "Singularité de l'artefact"
                                        },
                                        {
                                            "label": "Etalonnage",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": 515,
                                            "posy": 700,
                                            "posz": -1665,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Sur-mesure</b><br/>Textile<br/>Architecture<br/>Design<br/>Brevetage<br/>Norme d'applications<br/>Organisme<br/>Teste<br/><b>Etalonnage</b>",
                                            "synonymes": ["Etalonnage", "Calibrage"],
                                            "définition": "Normalisation de l'artefact"
                                        }
                                    ]

                                },
                                {
                                    "label": "Production en série",
                                    "group": 0x952222,
                                    "rang": 5,
                                    "posx": 45,
                                    "posy": 1180,
                                    "posz": -935,
                                    "synonymes": ["Production en série", "Industrie"],
                                    "définition": "Production rationalisée d'artefacts",
                                    "children": [
                                        {
                                            "label": "Artisanat",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": 290,
                                            "posy": 1615,
                                            "posz": -935,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Artisanat</b><br/>Création à la main<br/>Profit<br/>Richesse<br/><b>Industrie</b>",
                                            "synonymes": ["Artisanat"],
                                            "définition": "Echelle de production liée aux individus"
                                        },
                                        {
                                            "label": "Industrie",
                                            "group": 0x952222,
                                            "rang": 6,
                                            "posx": -120,
                                            "posy": 1630,
                                            "posz": -935,
                                            "cartabs": "Cartes Abstraites : <br/><br/><b>Artisanat</b><br/>Création à la main<br/>Profit<br/>Richesse<br/><b>Industrie</b>",
                                            "synonymes": ["Industrie", "Automatisation", "mécanisation", "standardisation", "spécialisation"],
                                            "définition": "Echelle de production liée aux structures"
                                        }
                                    ]
                                }
                            ]
            }
          ]
        }
      ]
    }
  ]
}
