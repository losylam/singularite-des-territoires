// Script pour schéma en cercle

// Définition de la dimension du SVG et du rayon du cercle
// Attention à prévoir de l'espace pour le texte vertical
var width = 1000,
    height = 1000,
    radius = .8 * Math.min(width, height) / 2;


// Generateurs d'arc (portions de couronne ou arc simple)
// Arc pour les sous-groupes (intégrité, reconversion...)
var arc = d3.svg.arc()
    .outerRadius(radius - 20)
    .innerRadius(radius - 40)
    .padAngle(0.01);

// Arc pour les délimiteurs (nouveauté, usage...)
var arcSpacer = d3.svg.arc()
    .outerRadius(radius)
    .innerRadius(0)
    .padAngle(0.01);

// Arc pour les indicateurs (Date de construction, Photos Flickr...)
var arcIndic = d3.svg.arc()
    .outerRadius(radius - 45)
    .innerRadius(radius - 300)
    .padAngle(0.01);

// Arc (ligne simple) pour les groupes (appropriation, tradition)
var arcArrow = d3.svg.arc()
    .outerRadius(radius - 5)
    .innerRadius(radius - 5)
    .padAngle(0.01);

// Générateur de "camembert" (permet le calcul des angles à partir des
// données CSV)
var pie = d3.layout.pie()
    .sort(null)
    .value(function (d) {
        // La valeur donne la taille relative de l'arc
        return d.value;
    });

// On ajoute le SVG et on décale la référence du repère au centre du rectangle.
var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

var data_csv, pie_data;

function create_pie_indic(data, pie_data) {
    var indics = data.filter(function (d) {
        return d.type == "indic"
    });
    var indics_by_groups = {}

    indics.forEach(function (d) {
        if (indics_by_groups.hasOwnProperty(d.group)) {
            indics_by_groups[d.group].indics.push(d)
        } else {
            var new_group = {};
            new_group.label = d.group;
            new_group.indics = [d];
            indics_by_groups[d.group] = new_group;
        }
    });

    console.log(indics_by_groups);

    pie_indic = [];

    pie_data.filter(function (d) {
            return d.data.type != "spacer"
        })
        .forEach(function (d) {
            var cur_group = indics_by_groups[d.data.subgroup]
            if (cur_group) {
                var start_angle = d.startAngle;
                var end_angle = d.endAngle;
                var num_indics = cur_group.indics.length;

                var delta_angle = (end_angle - start_angle) / num_indics;

                var cur_angle = start_angle;

                cur_group.indics.forEach(function (i) {
                    var new_indic = {
                        'data': i
                    }

                    new_indic.startAngle = cur_angle;
                    cur_angle += delta_angle;
                    new_indic.endAngle = cur_angle;
                    new_indic.padAngle = 0.0;

                    pie_indic.push(new_indic);
                })
            }
        })

    return pie_indic;
}


// Chargement des données des groupes et sous-groupes
d3.csv("groups.csv", type, function (error, data) {
    if (error) throw error;
    data_csv = data;

    pie_data = pie(data.filter(function (d) {
        return d.type != "indic";
    }));

    var pie_indics = create_pie_indic(data_csv, pie_data);

    console.log(pie_indics);

    // Création des arcs intérieurs (sous-groupes et délimiteurs)
    var g = svg.selectAll(".arc")
        .data(pie_data)
        .enter().append("g")
        .attr("class", "arc");

    // Choix du générateur d'arc en fonction du type
    g.append("path")
        .attr("d", function (d) {
            if (d.data.type === "spacer") {
                return arcSpacer(d);
            }
            if (d.data.type === "indic") {
                return arcIndic(d);
            } else {
                return arc(d);
            };
        })
        .attr("id", function (d, i) {
            return "subgroup" + i;
        })
        .attr("class", function (d) {
            if (d.data.type === "spacer") {
                return "spacer";
            }
            if (d.data.type === "indic") {
                return "indic";
            } else {
                return "part"
            }
        });

    // on crée un nouveau groupe svg pour les arcs des indicateurs
    var g_indics = svg.append('g')
        .attr('id', 'arc-indics')

    // dans ce groupe, on ajoute un groupe svg pour chaque indicateur
    var indics = g_indics.selectAll(".arc-indic")
        .data(pie_indics)
        .enter().append('g')
        .attr("class", "indic")
//        .on("mouseover", function (d) {
//            //Couleur des indicateurs
//            d3.select(this).select('.arc-indic').style("fill", function () {
//                if (d.data.group == "Reconversion"|| d.data.group == "Mutation") {
//                    return "#fff041"
//                }
//                if (d.data.group == "Labelisation" || d.data.group == "Inventaire") {
//                    return "rgb(96, 208, 255)"
//                }
//                if (d.data.group == "Dégradation" || d.data.group == "Obsolescence") {
//                    return "#01d954"
//                }
//                if (d.data.group == "Conservation" || d.data.group == "Transmission") {
//                    return "#e30613"
//                }
//                if (d.data.group == "Adhésion" || d.data.group == "Célébration") {
//                    return "#ae59b9"
//                }
//                if (d.data.group == "Innovation" || d.data.group == "Intégrité") {
//                    return "	#f9b233"
//                }
//            });
//        })
//	d3.select('.text-indic').style("fill",function(){
//
//	})
        .on("mouseout", function (d) {
            d3.select(this).select('.arc-indic').style("fill", "lightgray");
        });;

    // dans chacun de ces groupes, on ajoute un chemin 
    indics.append("path")
        .attr("class", "arc-indic")
        .attr("d", function (d) {
            return arcIndic(d);
        })

    // puis le du texte
    indics.append("text")
        .attr("class", "text-indic")
        .html(function (d) {
            return d.data.subgroup
        })
        .attr("transform", function (d) {
            angle = -90 + (d.startAngle + d.endAngle) * 90 / Math.PI;
            if (angle + 90 < 180) {
                return "rotate(" + angle +
                    ")translate(" + (radius - 60 ) +
                    ")";
            } else {
                return "rotate(" + angle +
                    ")translate(" + (radius - 60) +
                    ")rotate(180)"
            }
        })
        .attr("dy", "0.3em")
        .attr("text-anchor", function (d) {
            if ((d.startAngle + d.endAngle) / 2 < Math.PI) {
                return 'end';
            } else {
                return 'start';
            }
        });

    // Création d'un groupe SVG pour les futurs ajouts.
    var root = svg.append("g");

    // Ajout du texte à l'intérieur des arcs des délimiteurs.
    g.select(".spacer").each(function (d, i) {
        var rotate_angle = -90;

        // On tourne dans l'autre sens pour plus de visibilité quand on passe les
        // 180°.
        if ((d.startAngle + d.endAngle) / 2 > Math.PI) {
            rotate_angle = +90;
        }

        root.append("g")
            .attr("transform", "translate(" + arc.centroid(d) + ")")
            .attr("class", "group-text")
            .append("text")
            .text(d.data.group)
            .attr('transform', "rotate(" + (rotate_angle + ((d.startAngle + d.endAngle) / 2) * 180 / Math.PI) + ")")
            .attr('dy', '.3em')
            // Decalage du texte qui varie en fonction de sa rotation.
            .attr('dx', function () {
                if ((d.startAngle + d.endAngle) / 2 > Math.PI) {
                    return "-35px"
                }
                return '35px'
            })
            // Decalage du point de référence en fonction du choix de rotation.
            .attr("text-anchor", function () {
                if ((d.startAngle + d.endAngle) / 2 > Math.PI) {
                    return "end"
                }
                return "start"
            })

    });

    //A regular expression that captures all in between the start of a string
    //(denoted by ^) and the first capital letter L
    var firstArcSection = /(^.+?)L/;

    // Variable pour stocker les groupes (on "groupe" les groupes...)
    var groups = {};

    // Ajout du texte à l'intérieur des arcs des sous-groupes en suivant la
    // courbure.
    // Cela nécessite de créer un arc supplémentaire, invisible pour y
    // affecter le texte.
    g.select(".part").each(function (d, i) {
        //The [1] gives back the expression between the () (thus not the L as well)
        //which is exactly the arc statement
        var newArc = firstArcSection.exec(d3.select("#subgroup" + i).attr("d"))[1];
        //Replace all the comma's so that IE can handle it -_-
        //The g after the / is a modifier that "find all matches rather than
        //stopping after the first match"
        newArc = newArc.replace(/,/g, " ");

        // Si l'angle de début est supérieur à 90° et que l'angle de début est
        // inférieur à 270° on retourne le texte pour plus de lisibilité.
        if (d.startAngle > 90 * Math.PI / 180 && d.startAngle < 270 * Math.PI / 180) {
            //Everything between the capital M and first capital A
            var startLoc = /M(.*?)A/;
            //Everything between the capital A and 0 0 1
            var middleLoc = /A(.*?)0 0 1/;
            //Everything between the 0 0 1 and the end of the string (denoted by $)
            var endLoc = /0 0 1 (.*?)$/;
            //Flip the direction of the arc by switching the start and end point
            //and using a 0 (instead of 1) sweep flag
            var newStart = endLoc.exec(newArc)[1];
            var newEnd = startLoc.exec(newArc)[1];
            var middleSec = middleLoc.exec(newArc)[1];

            //Build up the new arc notation, set the sweep-flag to 0
            newArc = "M" + newStart + "A" + middleSec + "0 0 0 " + newEnd;
        } //if

        var text_group = root.append("g")
            .attr("class", "donut-arc")

        text_group.append("path")
            .attr("d", newArc)
            .attr("id", "donutArc" + i)
            .attr("class", "invisible")
        text_group.append("text")
            .attr("class", "subgroup-text")
            .attr("dy", function () {
                if (d.startAngle > 90 * Math.PI / 180 && d.startAngle < 270 * Math.PI / 180) {
                    return '-.35em';
                }
                return "1em"
            })
            // C'est le textPath qui permet au texte de suivre un chemin
            .append("textPath")
            .attr("xlink:href", "#donutArc" + i)
            .style("text-anchor", "middle")
            .attr("startOffset", "50%")
            .text(d.data.subgroup);

        // On groupe par group
        if (Object.keys(groups).indexOf(d.data.group) == -1) {
            groups[d.data.group] = {
                "name": d.data.group,
                "startAngle": d.startAngle
            }
        }

        groups[d.data.group]["endAngle"] = d.endAngle

    });

    // Création des flèches et du texte les accompagnant pour les groupes
    var arrows = svg.append("g")
        .attr("class", "arrows");

    // On crée une liste à partir d'un objet dictionnaire
    groups = Object.values(groups);

    // Ajout des groupes SVG pour chaque groupe
    var arrow_arc = arrows.selectAll(".arrow-arc").data(groups).enter()
        .append("g")
        .attr("class", "arrow-arc")
        .append("path")
        .attr("d", arcArrow)
        .attr("id", function (d, i) {
            return "group" + i
        });

    arrows.selectAll(".arrow-arc").each(function (d, i) {
        //The [1] gives back the expression between the () (thus not the L as well)
        //which is exactly the arc statement
        var newArc = firstArcSection.exec(d3.select("#group" + i).attr("d"))[1];

        // Pour la création des éléments des flèches (barre et pointe) on utilise
        // le "texte" du chemin de l'arc de référence et on le découpe pour
        // conserver les positions x,y de début et fin.
        var subpath = newArc.split(" ");
        var startPosition = subpath[0].split("A")[0].replace(/,/g, " ").replace("M", "");
        var endPosition = subpath[3].replace(/,/g, " ");
        //Replace all the comma's so that IE can handle it -_-
        //The g after the / is a modifier that "find all matches rather than
        //stopping after the first match"
        newArc = newArc.replace(/,/g, " ");

        // Si l'angle de début est supérieur à 90° et que l'angle milieu est
        // inférieur à 270° on retourne le texte.
        if (d.startAngle > 90 * Math.PI / 180 && (d.startAngle + d.endAngle) / 2 < 270 * Math.PI / 180) {
            //Everything between the capital M and first capital A
            var startLoc = /M(.*?)A/;
            //Everything between the capital A and 0 0 1
            var middleLoc = /A(.*?)0 0 1/;
            //Everything between the 0 0 1 and the end of the string (denoted by $)
            var endLoc = /0 0 1 (.*?)$/;
            //Flip the direction of the arc by switching the start and end point
            //and using a 0 (instead of 1) sweep flag
            var newStart = endLoc.exec(newArc)[1];
            var newEnd = startLoc.exec(newArc)[1];
            var middleSec = middleLoc.exec(newArc)[1];

            //Build up the new arc notation, set the sweep-flag to 0
            newArc = "M" + newStart + "A" + middleSec + "0 0 0 " + newEnd;
        } //if

        // Ajout du groupe SVG pour le texte
        var text_group = root.append("g")
            .attr("class", "group-arc-text")

        // Ajout de l'arc invisible pour servir de chemin au texte
        text_group.append("path")
            .attr("d", newArc)
            .attr("id", "groupArc" + i)
            .attr("class", "invisible")
        text_group.append("text")
            .attr("class", "group-text")
            .attr("dy", "-.35em")
            .attr("dy", function () {
                if (d.startAngle > 90 * Math.PI / 180 && (d.startAngle + d.endAngle) / 2 < 270 * Math.PI / 180) {
                    return '1em';
                }
                return "-.35em"
            })
            .append("textPath")
            .attr("xlink:href", "#groupArc" + i)
            .style("text-anchor", "middle")
            .attr("startOffset", "50%")
            .text(d.name);

        // Début (barre) de la flèche
        text_group.append("path")
            .attr("d", "M-5 0 L5 0")
            .attr("class", "line")
            .attr("transform", "translate(" + startPosition + "), rotate(" + ((-Math.PI / 2 + d.startAngle) * 180) / Math.PI + ")");
        // Fin (pointe) de la flèche
        text_group.append("path")
            .attr("d", "M-5 -5 L0 0 L5 -5")
            .attr("class", "line")
            .attr("transform", "translate(" + endPosition + "), rotate(" + ((-Math.PI / 2 + d.endAngle) * 180) / Math.PI + ")");

    });
});





function type(d) {
    return d;
}

function value() {
    return this.amount;
}
